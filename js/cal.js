
document.getElementById('start').innerHTML = " 0:00"
document.getElementById('stop').innerHTML = " 0:00"
document.getElementById('min').innerHTML = " 0 "
document.getElementById('riel').innerHTML = " 0 "

var startDay = new Date();
let start = document.getElementById('btn-start').onclick = () =>{
    document.getElementById("btn-start").style.display = "none";
    document.getElementById('btn-stop').style.display = "inline-block"
    let timePart = startDay.toLocaleTimeString().split(":");
    let donesplit = timePart[0] + ":" + timePart[1];
    document.getElementById('start').innerHTML = donesplit
}

let stop = document.getElementById('btn-stop').onclick = () =>{
    let stopDay = new Date();
    let timePart = stopDay.toLocaleTimeString().split(":");
    let donesplit = timePart[0] + ":" + timePart[1];
    document.getElementById('stop').innerHTML = donesplit
    let totalTime = stopDay - startDay;
    let totalMn = Math.floor(totalTime /60000)

    if(totalMn <= 15){
        totalMoney = 500 * totalMn;
    }
    else if(totalMn <=30){
        totalMoney = 1000 * totalMn;
    }
    else if(totalMn <=60){
        totalMoney = 1500 * totalMn;
    }
    else if(totalMn < 120){
        totalMn01 = totalMn - 60;
        if(totalMn01 <= 15){
            totalMoney = (500 * totalMn01) + (60* 1500);
        }
        else if(totalMn01 <=30){
            totalMoney = (1000 * totalMn01) + (60 * 1500);
        }
        else if(totalMn01 <=60){
            totalMoney = (1500 * totalMn01) + (60 * 1500);
        }
    }
    else{
        totalMoney = 1500 * totalMn
    }
    document.getElementById('min').innerHTML = totalMn
    document.getElementById('riel').innerHTML = totalMoney
    document.getElementById("btn-start").style.display = "none";
    document.getElementById('btn-stop').style.display = "none"
    document.getElementById('btn-clear').style.display = "inline-block"   
}


let liveTime = setInterval(() => {
    let now = new Date();
    let newTime = now.toString().split(" ");
    let datePart = newTime[0]+ " " + newTime[1] +" "+ newTime[2] +" "+ newTime[3];
    console.log(newTime);
    let timePart = now.toLocaleTimeString();
    document.getElementById('live-time').innerHTML = datePart +" "+ timePart;
}, 1000);

let clear = document.getElementById('btn-clear').onclick = () => {
    clearInterval(liveTime)
    document.getElementById('btn-start').style.display= "inline-block"
    document.getElementById("btn-clear").style.display = "none";
}